/*
  Whale(1) - File manipulating class
*/

package whalefile;

/**
 *
 * @author Whale(1) Corporation
 */
public abstract class WhaleFile
{
  private String file;

  public WhaleFile(String file){
    this.file = file;
  }

  protected boolean extensionIsValid(String extension){
    // If the extension is greater than file name, then this file have a invalid extension
    if(extension.length() > this.file.length()){
      return false;
    }
    // If file extension match with the validation extension
    int ini = this.file.length() - extension.length();
    int fin = this.file.length();
    if(!this.file.substring(ini, fin).toUpperCase().equals(extension.toUpperCase())){
        return false;
    }
    return true;
  }

  public String getFile(){
    return this.file;
  }

  public void setFile(String file){
    this.file = file;
  }
}
