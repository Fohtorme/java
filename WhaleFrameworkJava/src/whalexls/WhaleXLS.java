/*
  Whale(1) - XLS manipulating class
*/

package whalexls;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import whalefile.WhaleFile;

// Apache POI API
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author Whale(1) Corporation
 */
public class WhaleXLS extends WhaleFile
{
    private FileInputStream fileInput;
    private XSSFWorkbook workbook;
    private XSSFSheet sheet;
    
    
    public WhaleXLS(String file){
      super(file);
    }
    
    public void test(){
        try {
            this.fileInput = new FileInputStream(new File(super.getFile()));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WhaleXLS.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }
        try {
            this.workbook = new XSSFWorkbook(fileInput);
        } catch (IOException ex) {
            Logger.getLogger(WhaleXLS.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }
        this.sheet = workbook.getSheetAt(0);
        
        for(Row row : this.sheet){
            for(Cell cell : row){
                System.out.println("Teste:" + cell);
            }
            
        }
    }
  
    public boolean extensionIsValid(){
        if(super.extensionIsValid(".XLS"))
            return true;
        if(super.extensionIsValid(".XLSX"))
            return true;
        return false;
    }
}
